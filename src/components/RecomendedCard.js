import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { Food } from '../assets'
import { colors } from '../utils'
import Icon from 'react-native-vector-icons/Ionicons'
import { TouchableOpacity } from 'react-native-gesture-handler'

const RecomendedCard = ({ price, name, img, recommended, recommendedPress, addPress, detailPress }) => {
    return (
        <View style={styles.wrapper}>
            <TouchableOpacity onPress={detailPress} >
                <Image
                    style={styles.image}
                    source={{ uri: `${img}` }}
                    height={80}
                    width={80}
                />
            </TouchableOpacity>
            <View style={styles.icon}>
                <TouchableOpacity onPress={recommendedPress}>
                    {recommended?<Icon name='ios-heart' size={30} color={colors.primary} />:<Icon name='ios-heart' size={30} color={'grey'} />}
                </TouchableOpacity>
            </View>
            <Text style={styles.text}>{name}</Text>
            <View style={styles.harga}>
                <Text style={styles.text2}>Rp. {price}</Text>
                <TouchableOpacity onPress={addPress}>
                    <Icon name='ios-add-circle' size={25} color={colors.primary} />
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default RecomendedCard

const styles = StyleSheet.create({
    wrapper: {
        width: 152,
        height: 183,
        borderRadius: 16,
        borderColor: 'black',
        borderWidth: 2,
        marginRight: 10,
        elevation: 10,
        backgroundColor: colors.background
    },
    image: {
        marginTop: 22,
        marginBottom: 8,
        alignSelf: 'center',
        width: 80,
        height: 80,
        borderRadius: 40
    },
    icon: {
        position: 'absolute',
        right: 5,
        top: 5
    },
    text: {
        alignSelf: 'center',
        justifyContent:'center',
        // fontWeight: 'bold',
        fontSize: 12,
        marginBottom: 10,
        color: 'black'
    },
    text2: {
        justifyContent: 'center',
        fontWeight: 'bold',
        fontSize: 12,
        marginBottom: 10,
        color: colors.primary,
    },
    harga: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
    }
})
