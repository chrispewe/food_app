import React, { useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'

const Option = ({ options, onChange }) => {
    const [activeOption, setActiveOption] = useState('hottest')

    return (
        <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
            <ScrollView horizontal>
                {options.map((option, index) => (
                    <TouchableOpacity
                        key={index}
                        onPress={() => {
                            onChange(option, index);
                            setActiveOption(option)
                        }}
                    >
                        <Text
                            style={styles.text(activeOption, option)}
                        >
                            {option}
                        </Text>
                    </TouchableOpacity>
                ))}
            </ScrollView>
        </View>
    )
}

export default Option

const styles = StyleSheet.create({
    text: (activeOption, option) => {
        return {
            paddingVertical: 7,
            textAlign: 'center',
            fontWeight: activeOption === option ? 'bold' : '200',
            fontSize: activeOption === option ? 15 : 13,
            width: 95,
            height: 40,
            marginVertical: 5,
            marginHorizontal: 2,
            color: activeOption === option ? 'red' : 'black',
            borderWidth: 1,
            borderRadius: 20
        }
    }
})
