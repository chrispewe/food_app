import React, { useEffect } from 'react'
import { StyleSheet, View } from 'react-native'
import { SplashImage } from '../../assets';
import { colors } from '../../utils';
import * as Animatable from 'react-native-animatable';
import AsyncStorage from '@react-native-async-storage/async-storage';
import queryFood from '../../api/foodApi';

const SplashScreen = (props) => {

    useEffect(() => {
        setTimeout(async() => {
            getData()
            // props.navigation.reset('WelcomeScreen')
        }, 3000);
    },[]);


    const getData = async () => {
        try {
            const token = await AsyncStorage.getItem('@token')
            const name = await AsyncStorage.getItem('@name')
            if (token) {
                const result = await queryFood(token,name);
                // console.log(result)

                if (result.status === 200) {
                    props.navigation.reset({
                        index: 0,
                        routes: [{ name: 'HomeScreen' }],
                    })
                } else {
                    props.navigation.reset({
                        index: 0,
                        routes: [{ name: 'WelcomeScreen' }],
                    })
                }
            } else {
                props.navigation.reset({
                    index: 0,
                    routes: [{ name: 'WelcomeScreen' }],
                })
            }


        } catch (e) {
            console.warn(e)
        }
    }

    return (
        <View style={styles.wrapper}>
            <Animatable.Image
            duration={2000}
            animation="tada" 
            easing="ease-in-sine"
            // easing="ease-out" 
            iterationCount="infinite"
            direction='alternate' 
            style={styles.image} 
            source={SplashImage} 
            // height={300} 
            // width={300} 
            />
        </View>
    )
}

export default SplashScreen

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: colors.white,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        width: 184,
        height: 205.05
    }
})
