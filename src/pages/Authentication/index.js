import React, { useEffect, useState } from 'react'
import { Dimensions, Image, StyleSheet, Text, View } from 'react-native'
import { colors } from '../../utils'
import { AuthenticationImage, AuthenticationImageShadow, WelcomeScreenImageSmall } from '../../assets';
import * as Animatable from 'react-native-animatable';
import { Button, Input } from '../../components';
import { ScrollView } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage'
import { connect } from 'react-redux';
import fetchUser from '../../stores/actions/userAction';
import fetchFood from '../../stores/actions/foodAction';

const { width, height } = Dimensions.get('window');

const Authentication = (props) => {
    const [name, setName] = useState('ricko');

    const handleUser = async () => {
        try {
            props.dispatchUser(name);

        } catch (e) {
            alert(e)
            console.error(e);
        }
    };

    useEffect(() => {
        if (props.userStore.payload.data&& props.userStore.payload.data !== '') {          
            const saveData = async () => {
                try {
                    await AsyncStorage.setItem('@token', props.userStore.payload.data.token)
                    await AsyncStorage.setItem('@name', props.userStore.payload.data.username)
                    // await props.dispatchFood(props.userStore.payload.data.token, props.userStore.payload.data.username)
                    // console.warn(`token ${props.userStore.payload.data.token}successfully saved to async storage`)
                } catch (e) {
                    console.warn('Failed to save the data to the async storage')
                }
            }
            saveData()
            {!props.foodStore.isLoading?props.navigation.navigate('HomeScreen'):null}
        }
    }, [props.userStore.payload.data])


    return (
        <ScrollView style={styles.wrapperAtas}>
            <Animatable.View
                duration={2000}
                animation="zoomInDown"
                // animation="pulse"
                easing="ease-in-sine"
                // easing="ease-out" 
                // iterationCount="infinite"
                direction='alternate'
                style={{ width: width, height: 335 }}
            >
                <Image
                    style={styles.imageSmall}
                    source={WelcomeScreenImageSmall}
                    height={37.5}
                    width={50}
                />
                <Image
                    style={styles.image}
                    source={AuthenticationImage}
                    height={260}
                    width={301}
                />
                <Image
                    style={styles.image}
                    source={AuthenticationImageShadow}
                    height={12}
                    width={301}
                />
            </Animatable.View>

            <Animatable.View
                duration={1000}
                animation="fadeInUp"
                // animation="pulse"
                easing="ease-in-sine"
                // easing="ease-out" 
                // iterationCount="infinite"
                direction='alternate'
                style={styles.wrapperBawah}
            >
                <Text style={styles.text1}>What is your firstname?</Text>
                <View style={{ height: 16 }} />
                <Input
                    type="name"
                    placeholder="name"
                    value={name}
                    onChangeText={(value) => setName(value)}
                />
                <View style={{ height: 40 }} />
                <Button title="Start Ordering" onPress={() => handleUser({name})} type="default" />
            </Animatable.View>

        </ScrollView>
    )
}

function mapStateToProps(state) {
    return {
        userStore: state.userStore,
        foodStore: state.foodStore,
    }
}

function mapDispatchToProps(dispatch) {
    return {        
        dispatchFood: (token,name) => dispatch(fetchFood(token,name)),
        dispatchUser: (name) => dispatch(fetchUser(name)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Authentication)

const styles = StyleSheet.create({
    wrapperAtas: {
        backgroundColor: colors.primary,
        flex: 1,
        paddingTop: 134
    },
    wrapperBawah: {
        alignSelf: 'center',
        backgroundColor: colors.background,
        width: width,
        height: 400,
        // flex: 1,
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        paddingHorizontal: 25
    },
    image: {
        alignSelf: 'center',
        marginBottom: 8
    },
    imageSmall: {
        position: 'absolute',
        alignSelf: 'center',
        right: 40
    },
    text1: {
        marginTop: 40,
        fontSize: 18,
        fontWeight: 'bold',
    },
    text2: {
        marginTop: 20,
        fontSize: 14,
        fontWeight: '400',
    }

})
