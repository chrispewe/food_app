import AuthenticationImage from './images/AuthenticationImage.png';
import AuthenticationImageShadow from './images/AuthenticationImageShadow.png'
import Food from './images/Food.png'
import SplashImage from './images/SplashImage.png'
import WelcomeScreenImage from './images/WelcomeScreenImage.png'
import WelcomeScreenImageShadow from './images/WelcomeScreenImageShadow.png'
import WelcomeScreenImageSmall from './images/WelcomeScreenImageSmall.png'

export {WelcomeScreenImageSmall,WelcomeScreenImageShadow,WelcomeScreenImage,SplashImage,AuthenticationImage,AuthenticationImageShadow,Food,}
